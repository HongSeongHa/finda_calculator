package util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    fun getCalendarForMidnight(): Calendar {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 0 // set hour to midnight
        cal[Calendar.MINUTE] = 0 // set minute in hour
        cal[Calendar.SECOND] = 0 // set second in minute
        cal[Calendar.MILLISECOND] = 0 // set millisecond in second
        return cal
    }

    fun getCalendarForMidnight(epochTime: Long): Calendar {
        val cal = Calendar.getInstance()
        cal.timeInMillis = epochTime
        cal[Calendar.HOUR_OF_DAY] = 0 // set hour to midnight
        cal[Calendar.MINUTE] = 0 // set minute in hour
        cal[Calendar.SECOND] = 0 // set second in minute
        cal[Calendar.MILLISECOND] = 0 // set millisecond in second
        return cal
    }

    fun getDayDiff(prevTimestamp: Long, postTimestamp: Long): Long {
        val prevTime = getCalendarForMidnight(prevTimestamp)
        val postTime = getCalendarForMidnight(postTimestamp)
        return (postTime.timeInMillis - prevTime.timeInMillis) / 1000 / (60 * 60 * 24)
    }

    fun getMonthDiff(prevTimestamp: Long, postTimestamp: Long): Int {
        val prevTime = getCalendarForMidnight(prevTimestamp)
        val postTime = getCalendarForMidnight(postTimestamp)
        val prevMonth =
            prevTime[Calendar.YEAR] * 12 + prevTime[Calendar.MONTH]
        val postMonth =
            postTime[Calendar.YEAR] * 12 + postTime[Calendar.MONTH]
        return postMonth - prevMonth
    }

    fun getFormattedDate(
        epochTime: Long?,
        pattern: String?,
        addDayOfWeek: Boolean? = false
    ): String? {
        if (epochTime == null || epochTime == 0L) return null
        val seoul = TimeZone.getTimeZone("Asia/Seoul")
        val sdf = SimpleDateFormat(pattern, Locale.KOREA)
        sdf.timeZone = seoul
        val date = Date(epochTime)
        val result = sdf.format(date)
        return if (addDayOfWeek == true) result + getDayOfWeekKr(epochTime) else result
    }

    private fun getDayOfWeekKr(epochTime: Long): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date(epochTime)
        var result = ""
        when (calendar[Calendar.DAY_OF_WEEK]) {
            1 -> result = "일"
            2 -> result = "월"
            3 -> result = "화"
            4 -> result = "수"
            5 -> result = "목"
            6 -> result = "금"
            7 -> result = "토"
        }
        return result
    }

    fun getWeekOfMonth(epochTime: Long?): String {
        if (epochTime == null) return ""
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epochTime
        return calendar[Calendar.WEEK_OF_MONTH].toString()
    }

    fun todayDateEpochTime(): Long {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val currentTime = simpleDateFormat.format(Date())
        try {
            val date = simpleDateFormat.parse(currentTime)
            return date.time
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return -1
    }

    fun getDayOfWeekNumber(epochTime: Long): Int {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epochTime
        return calendar[Calendar.DAY_OF_WEEK]
    }

    /** 현재일로 */
    fun getMonth(epochTime: Long): Int {
        val cal = Calendar.getInstance()
        cal.time = Date(epochTime)
        return cal[Calendar.MONTH]
    }

    fun getDay(epochTime: Long): Int {
        val cal = Calendar.getInstance()
        cal.time = Date(epochTime)
        return cal[Calendar.DATE]
    }
}