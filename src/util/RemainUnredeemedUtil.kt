package util

import model.calc.CalcAccount
import java.util.*

object RemainUnredeemedUtil {
    fun getRemainUnredeemedPeriod(
        calcAccount: CalcAccount,
        isCustomAccount: Boolean?
    ): Long {
        calcAccount.apply {
            if (isCustomAccount!!) return 0L

            val calendar = Calendar.getInstance()
            calendar.time = Date()

            val openDateCalendar = DateUtil.getCalendarForMidnight(openDate)
            val openDay = openDateCalendar[Calendar.DATE]
            val openMonth = openDateCalendar[Calendar.MONTH] + 1
            val openYear = openDateCalendar[Calendar.YEAR]

            val dueDateCalendar = DateUtil.getCalendarForMidnight(dueDate)
            val dueDay = dueDateCalendar[Calendar.DATE]
            val dueMonth = dueDateCalendar[Calendar.MONTH] + 1
            val dueYear = dueDateCalendar[Calendar.YEAR]

            var totalRepayMonth: Long = 0 // 전체 기간

            if (repayDay > openDay) {
                totalRepayMonth += (dueYear - openYear) * 12 + (dueMonth - openMonth).toLong()
                totalRepayMonth += if (dueDay - repayDay <= 0) 0 else -1
            } else {
                totalRepayMonth += (dueYear - openYear) * 12 + (dueMonth - openMonth).toLong()
                totalRepayMonth += if (dueDay - repayDay <= 0) 1 else 0
            }

            val result = remainMonth + unredeemedPeriod - totalRepayMonth
            return if (result < 0) 0 else result
        }
    }
}