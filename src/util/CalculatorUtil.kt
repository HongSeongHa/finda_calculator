package util

import data.LoanManageAccount
import model.calc.CalcAccount
import model.calc.CalcResult
import kotlin.math.pow

class CalculatorUtil(private val account: CalcAccount) {
    fun calculate(): List<CalcResult> {
        return when (account.loanType) {
            LoanManageAccount.LOAN_TYPE_EQUAL_PAY -> getLevelMonthlyList()
            LoanManageAccount.LOAN_TYPE_EQUAL_ORIGIN -> getLevelPrincipalMonthlyList()
            else -> getBulletMonthlyList()
        }
    }

    /** 원리금 균등 상환 월별 원금, 이자, 총액 데이터 리스트 계산 및 리턴 함수*/
    private fun getLevelMonthlyList(): List<CalcResult> {
        /** 월별 원금, 이자, 총액 데이터를 담고 있는 리스트 */
        val debtMonthlyList = mutableListOf<CalcResult>()

        /** 월 상환금 (월 원금 + 이자) 계산 로직 */
        val repayment: Double

        /** 원금과 이자를 구하기 위한 대출잔금 변수 */
        var debtBalance: Long

        account.apply {
            debtBalance = loanAmount

            repayment = loanAmount * monthlyRate * (1 + monthlyRate)
                .pow((remainMonth - unredeemedPeriod).toDouble()) / ((1 + monthlyRate)
                .pow((remainMonth - unredeemedPeriod).toDouble()) - 1)

            repeat(remainMonth) {
                if (it >= unredeemedPeriod) {
                    val debt = CalcResult()

                    debt.total = repayment.toLong()
                    debt.interest = (debtBalance * monthlyRate).toLong()
                    debt.principal = debt.total - debt.interest

                    debtBalance -= debt.principal
                    debt.leftBalance = debtBalance

                    debtMonthlyList.add(debt)
                } else {
                    val debt = CalcResult()
                    debt.total = 0
                    debt.interest = (debtBalance * monthlyRate).toLong()
                    debt.principal = 0
                    debt.leftBalance = debtBalance

                    debtMonthlyList.add(debt)
                }
            }
        }

        return debtMonthlyList
    }

    /** 원금 균등 상환 월별 원금, 이자, 총액 데이터 리스트 계산 및 리턴 함수 */
    private fun getLevelPrincipalMonthlyList(): List<CalcResult> {
        /** 월별 원금, 이자, 총액 데이터를 담고 있는 리스트 */
        val debtMonthlyList = mutableListOf<CalcResult>()

        /** 월 납입원금 */
        val principal: Long

        /** 원금과 이자를 구하기 위한 대출잔금 변수 */
        var debtBalance: Long

        account.apply {
            debtBalance = loanAmount

            principal = loanAmount / (remainMonth - unredeemedPeriod)

            repeat(remainMonth) {
                if (it >= unredeemedPeriod) {
                    val debt = CalcResult()

                    debt.principal = principal
                    debt.interest = (debtBalance * monthlyRate).toLong()
                    debt.total = debt.principal + debt.interest

                    debtBalance -= debt.principal
                    debt.leftBalance = debtBalance

                    debtMonthlyList.add(debt)
                } else {
                    val debt = CalcResult()

                    debt.total = 0
                    debt.interest = (debtBalance * monthlyRate).toLong()
                    debt.principal = 0
                    debt.leftBalance = debtBalance

                    debtMonthlyList.add(debt)
                }
            }
        }
        return debtMonthlyList
    }

    /** 만기 일시 상환 월별 원금, 이자, 총액 데이터 리스트 계산 및 리턴 함수 */
    private fun getBulletMonthlyList(): List<CalcResult> {
        /** 월별 원금, 이자, 총액 데이터를 담고 있는 리스트 */
        val debtMonthlyList = mutableListOf<CalcResult>()

        /** 월 납입 이자 */
        var interest: Long

        /** 만기 일시 상환 특성상 거치기간의 의미가 없다
         * 따라서 만기 일시 상환 로직에서는 거치기간을 비교하는 코드는 없음. */
        account.apply {
            interest = (loanAmount * monthlyRate).toLong()

            repeat(remainMonth) {
                if (it != remainMonth - 1)
                    debtMonthlyList.add(CalcResult(interest, 0, interest, loanAmount))
                else
                    debtMonthlyList.add(CalcResult(interest, loanAmount, interest + loanAmount, 0))
            }
        }

        return debtMonthlyList
    }
}