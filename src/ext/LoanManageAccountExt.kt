package ext

import data.LoanManageAccount

fun List<LoanManageAccount>.excludeFilterByLoanType(
    excludeLoanTypeList: List<String> = listOf(
        LoanManageAccount.LOAN_TYPE_MINUS
    )
): List<LoanManageAccount> =
    this.filter {
        !excludeLoanTypeList.contains(it.loanType)
    }