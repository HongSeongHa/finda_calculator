package data

import util.DateUtil
import java.util.*

class LoanManageAccount {

    val balanceAmount: Long? = null
    val accountNumber: String? = null
    val agreeAmount: Long? = null
    val bank: Bank? = null
    val bankName: String? = null
    val checkBalanceAmount: Long? = null
    val checkMonth: String? = null
    val checkRepayAmount: Long? = null
    val id: Long = 0
    val insertTime: Long = 0
    val dueDate: Long? = null
    val openDate: Long? = null
    val overdueAmount: Long? = null
    val productName: String? = null
    val monthlyRepayList: List<LoanMonthlyInfo>? = null

    val unredeemedPeriod: Int? = null
    val unredeemedPeriodC: Int? = null
    val unredeemedPeriodI: Int? = null
    val unredeemedPeriodSelect: String? = null
    var isEnable = true
    val updateTime: Long = 0
    val loanRate: Double? = null
    val loanRateC: Double? = null
    val loanRateI: Double? = null
    val loanRateS: Double? = null
    val loanRateSelect: String? = null
    val loanType: String? = null
    val loanTypeI: String? = null
    val loanTypeK: String? = null
    val loanTypeSelect: String? = null
    val repayDay: Int? = null
    val repayDayI: Int? = null
    val repayDayS: Int? = null
    val repayDaySelect: String? = null

    /** 상품의 남은 상환 기간 */
    val remainRepayPeriod: Int
        get() = monthlyRepayList?.size ?: 0

    /**
     * @param targetPeriod    현재일 기준으로 '얼마까지 기간'의 총 합을 구할지 판단하기 위한 인자
     * @param repayResultType 총 합이 '원금+이자', '이자'가 될것인지 확인 하는 구분자
     * @return 일정 기간동안의 '원금+이자' 또는 '이자'의 총합
     */
    fun getTotalLoanRepayAmount(targetPeriod: Int?, repayResultType: RepayResultType?): Int {
        if (monthlyRepayList == null || targetPeriod == null) return 0
        val periodRange =
            if (monthlyRepayList.size < targetPeriod) monthlyRepayList.size else targetPeriod
        var totalAmount = 0
        for (i in 0 until periodRange) {
            val loanMonthlyInfo = monthlyRepayList[i]
            totalAmount += loanMonthlyInfo.interest.toInt()
            val interestOnly = repayResultType === RepayResultType.INTEREST
            if (!interestOnly) {
                totalAmount += loanMonthlyInfo.principal.toInt()
            }
        }
        return totalAmount
    }

    // 계좌가 잠겨있는지 반환
    val isAccountLocked: Boolean
        get() = loanRate == null || loanType == null || loanType == LOAN_TYPE_UNKNOWN || unredeemedPeriod == null

    // 입력 안된 추가정보 갯수
    val lockedAccountExtraInfoCount: Int
        get() {
            var count = 0
            if (loanRate == null || loanRate == 0.0) {
                count++
            }
            if (loanType == null || LOAN_TYPE_분할상환 == loanType) {
                count++
            }
            if (unredeemedPeriod == null) {
                count++
            }
            if (repayDay == null || repayDay == 0) {
                count++
            }
            return count
        }

    // 다음 상환일 계산 (다음 상환월 + 입력된 상환일)
    fun getNextRepayDay(): Long? {
        if (loanType == LOAN_TYPE_MINUS) {  // 마이너스 통장일 때
            val calendar = Calendar.getInstance()
            calendar.time = Date(System.currentTimeMillis())
            repayDay?.let {
                val today = calendar.get(Calendar.DATE)
                if (today > repayDay) calendar.add(Calendar.MONTH, +1)
                calendar.set(Calendar.DATE, repayDay)
            }
            return calendar.timeInMillis
        } else {
            val loanMonthlyInfo = getCurrentMonthlyInfo() ?: return null
            val date = loanMonthlyInfo.repayDate
            val cal = DateUtil.getCalendarForMidnight(date)
            return cal.timeInMillis
        }
    }

    //  다음 상환일 까지의 남은 기간
    fun getNextRepayDayDiff(): Long? {
        val nextRepayDay = getNextRepayDay()
        return nextRepayDay?.let {
            DateUtil.getDayDiff(System.currentTimeMillis(), it)
        }
    }

    //  다음 상환 금액 계산
    fun getNextRepayAmount(): Long? {
        val loanMonthlyInfo = getCurrentMonthlyInfo() ?: return null
        return loanMonthlyInfo.principal + loanMonthlyInfo.interest
    }

    // monthly info의 첫번째 상환 일정이 오늘 이후인지 이전인지 계산 후 다음 일정 반환
    private fun getCurrentMonthlyInfo(): LoanMonthlyInfo? {
        if (monthlyRepayList.isNullOrEmpty()) return null
        val cal = DateUtil.getCalendarForMidnight(monthlyRepayList[0].repayDate)
        repayDay?.let {
            cal[Calendar.DATE] = it
        }
        val todayCal = DateUtil.getCalendarForMidnight(System.currentTimeMillis())
        val havePastRepayInfo = cal.timeInMillis < todayCal.timeInMillis
        return if (monthlyRepayList.size <= 1 || !havePastRepayInfo) monthlyRepayList[0] else monthlyRepayList[1]
    }

    companion object {
        // loan Type
        const val LOAN_TYPE_ONCE = "일시상환"
        const val LOAN_TYPE_분할상환 = "분할상환"
        const val LOAN_TYPE_MINUS = "한도대출"
        const val LOAN_TYPE_EQUAL_ORIGIN = "원금균등상환"
        const val LOAN_TYPE_EQUAL_PAY = "원리금균등상환"
        const val LOAN_TYPE_UNKNOWN = "UNKNOWN"

        // 추가 정보로써 수정이 가능한 properties
        const val TYPE_EDITABLE_REPAY_DAY = "RepayDay"
        const val TYPE_EDITABLE_UNREDEEMED_PERIOD = "UnredeemedPeriod"
        const val TYPE_EDITABLE_LOAN_TYPE = "LoanType"
        const val TYPE_EDITABLE_LOAN_RATE = "LoanRate"
    }
}

class LoanMonthlyInfo(var principal: Long, var interest: Long) {
    val repayDate: Long = 0
    val month: Long = 0
}

enum class RepayResultType(val displayName: String) {
    PRINCIPLE_AND_INTEREST("원금+이자"),
    INTEREST("이자");

    companion object {
        val repayResultTypeList: MutableList<String>
            get() {
                return mutableListOf(PRINCIPLE_AND_INTEREST.displayName, INTEREST.displayName)
            }

        fun fromDisplayName(displayName: String): RepayResultType {
            return values().find {
                displayName.contains(it.displayName)
            } ?: INTEREST
        }
    }
}

class Bank {
    var id = 0
    var cooconModule: String? = null
    var shortName: String? = null
    var fullName: String? = null
    var logoUrl: String? = null
    var cooconModuleName: String? = null
    var loanApplyNotes: String? = null
    var loanApplyBridgeImgUrl: String? = null
}