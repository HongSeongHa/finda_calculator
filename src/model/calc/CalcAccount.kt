package model.calc

import data.LoanManageAccount

/**
 * 가장 기본인 CalcAccount를 이용하는 방법입니다.
 * 단순히 계산을 위해서는 아래 5가지만 충족하면 됩니다.
 *
 * @property loanType 대출 방식을 의미. UNKNOWN으로 초기화되어 있으므로 설정 필수.
 * @property loanAmount 대출 총액을 의미함. 설정 필수.
 * @property yearlyRate 금리를 의미함. 월금리는 계산 로직에서 자동으로 사용하므로 필요 없음.
 * @property remainMonth 잔여기간을 의미함. 올바른 값이 도출되기 위해서는 설정해야 함.
 * @property unredeemedPeriod 거치기간을 의미함. 기본값은 0이므로 거치기간이 존재하지 않으면 설정하지 않아도 됨
 *
 * Debt, Advance를 계산하기 위한 경우에는 아래 3가지를 포함한 총 8가지를 모두 충족해야 합니다.
 * @property openDate 대출일
 * @property dueDate 만기일
 * openDate와 dueDate는 epoch time으로 대출일과 만기일을 도출해내기 위한 Long 타입 변수입니다.
 * @property repayDay
 * repayDay는 상환일을 의미하여, 각 월의 상환일을 의미합니다.
 *
 * @author 홍성하
 */
open class CalcAccount {
    open var loanType: String? = LoanManageAccount.LOAN_TYPE_UNKNOWN
    open var loanAmount: Long = 0
    open var yearlyRate: Double = 0.0
    open val monthlyRate: Double
        get() = yearlyRate / 12

    open var remainMonth: Int = 0
    open var unredeemedPeriod: Int = 0

    open var openDate: Long = 0
    open var dueDate: Long = 0
    open var repayDay = 0
}