package model.calc

data class CalcResult(
    var interest: Long = 0,
    var principal: Long = 0,
    var total: Long = 0,
    var leftBalance: Long = 0
) {

    override fun toString(): String {
        return "납입원금 : $principal, 대출이자 : $interest, 월상환금 : $total, 대출잔금 : $leftBalance"
    }
}