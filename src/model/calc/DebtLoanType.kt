package model.calc

import java.io.Serializable

enum class DebtLoanType(val displayName: String) : Serializable {
    MORTGAGE_LOAN("주택담보대출"),
    PERSONAL_LOAN("신용대출"),
    RENT_LOAN("전세자금대출"),
    RENT_DEPOSIT_BASED_LOAN("전세보증금 담보대출"),
    MINUS("마이너스통장"),
    STUDENT_LOAN("학자금대출"),
    MOVING_LOAN("이주비대출"),
    MIDDLE_PAYMENT_LOAN("중도금대출"),
    REMAIN_PAYMENT_LOAN("잔금대출"),
    INSURANCE_BASED_LOAN("보험계약대출"),
    STOCK_BASED_LOAN("유가증권담보대출(스탁론)"),
    DEPOSIT_BASED_LOAN("예적금 담보대출"),
    LEASE("할부금융/리스"),
    ETC_LOAN("그 외 기타대출");
}