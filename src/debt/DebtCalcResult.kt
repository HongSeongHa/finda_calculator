package debt

class DebtCalcResult {
    // 연 소득
    var yearlyIncome: Long? = null

    // 대출 계좌 리스트
    var debtCalcAccountList: List<DebtCalcAccount>? = null

    // DTI, DSR 계산의 최근 시간
    var calculatedTime: Long = System.currentTimeMillis()


    // DTI
    var dti: Double? = null

    // DTI 주택담보대출 연 원리금 (모기지론 총 원리금)
    var mortgageLoanDtiYearlyRepay: Long? = null

    // DTI 기타 대출 연 이자
    var etcLoanDtiYearlyRepay: Long? = null

    // DTI 계산용 원리금
    var dtiTotalRepay: Long? = null


    // DSR
    var dsr: Double? = null

    // DSR 주택담보대출 연 원리금
    var mortgageLoanDsrYearlyRepay: Long? = null

    // DSR 기타 대출 연 원리금
    var etcLoanDsrYearlyRepay: Long? = null

    // DSR 계산용 원리금
    var dsrTotalRepay: Long? = null

    override fun toString(): String {
        return """연 소득 : $yearlyIncome
            |DTI : $dti
            |DTI 주택담보대출 연 원리금 : $mortgageLoanDtiYearlyRepay
            |DTI 기타대출 연 이자 : $etcLoanDtiYearlyRepay
            |DTI 계산용 원리금 : $dtiTotalRepay
            |DSR : $dsr
            |DSR 주택담보대출 연 원리금 : $mortgageLoanDsrYearlyRepay
            |DSR 기타대출 연 원리금 : $etcLoanDsrYearlyRepay
            |DSR 계산용 원리금 : $dsrTotalRepay
            |
        """.trimMargin()
    }
}