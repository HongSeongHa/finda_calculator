package debt

import data.LoanManageAccount
import ext.sumBy
import model.calc.CalcResult
import model.calc.DebtLoanType
import util.CalculatorUtil
import util.RemainUnredeemedUtil
import kotlin.math.roundToInt

class DebtCalculator(
    private val debtCalcAccounts: List<DebtCalcAccount>?,
    private val yearlyIncome: Long?
) {
    fun calculate(): DebtCalcResult {
        val debtCalcResult = DebtCalcResult().apply {
            yearlyIncome = this@DebtCalculator.yearlyIncome
            calculatedTime = System.currentTimeMillis()
            debtCalcAccountList = debtCalcAccounts
        }

        debtCalcAccounts?.forEach { debtAccountCalculate(it) }

        debtCalcResult.apply {
            dti = calcDti(debtCalcAccounts, yearlyIncome ?: 0)
            dsr = calcDsr(debtCalcAccounts, yearlyIncome ?: 0)

            mortgageLoanDtiYearlyRepay =
                calcDtiYearlyRepay(debtCalcAccounts?.filter { it.debtLoanType == DebtLoanType.MORTGAGE_LOAN })
            etcLoanDtiYearlyRepay =
                calcDtiYearlyRepay(debtCalcAccounts?.filter { it.debtLoanType != DebtLoanType.MORTGAGE_LOAN })
            dtiTotalRepay = calcDtiTotalRepay(debtCalcAccounts)

            mortgageLoanDsrYearlyRepay =
                calcDsrYearlyRepay(debtCalcAccounts?.filter { it.debtLoanType == DebtLoanType.MORTGAGE_LOAN })
            etcLoanDsrYearlyRepay =
                calcDsrYearlyRepay(debtCalcAccounts?.filter { it.debtLoanType != DebtLoanType.MORTGAGE_LOAN })
            dsrTotalRepay = calcDsrTotalRepay(debtCalcAccounts)
        }

        return debtCalcResult
    }

    private fun debtAccountCalculate(debtCalcAccount: DebtCalcAccount) {
        /**
         * debtCalcAccount에 대응하는 loanCalcAccount를 만든후 각 값을 바인딩한다
         * */

        debtCalcAccount.unredeemedPeriod =
            RemainUnredeemedUtil.getRemainUnredeemedPeriod(
                debtCalcAccount,
                debtCalcAccount.isCustomAccount
            ).toInt()

        /**
         * subList(unredeemedPeriod, unredeemedPeriod + 12)를 하는 이유는 1년간의 데이터로만 측정하기 때문
         * dti와 dsr은 연 소득 대비로 계산하기 때문에 모든 대출에 대해서도 연 상환액을 기준으로 하기 때문.
         * */

        val loanCalcResults = CalculatorUtil(debtCalcAccount).calculate()

        val resizeResult = loanCalcResults
            .subList(debtCalcAccount.unredeemedPeriod,
                if (12 + debtCalcAccount.unredeemedPeriod >= loanCalcResults.size)
                    loanCalcResults.size
                else
                    12 + debtCalcAccount.unredeemedPeriod
            )

        debtCalcAccount.apply {
            yearlyPrinciple = resizeResult.sumBy { it.principal }.toDouble()
            yearlyInterest = resizeResult.sumBy { it.interest }.toDouble()

            /**
             * DTI는 모기지론(주택담보대출)만 원리금, 이외 대출은 이자만
             * DSR은 모든 대출을 원리금으로 계산, 하지만 대출 방식마다 원금 계산 방식이 다르기 때문에 주의!
             */
            dtiRepayAmount = getCalcDtiRepayAmount(this, resizeResult)
            dsrRepayAmount = getCalcDsrRepayAmount(this, resizeResult)
        }
    }

    private fun getCalcDtiRepayAmount(debtCalcAccount: DebtCalcAccount, loanCalcResult: List<CalcResult>): Double {
        debtCalcAccount.apply {
            return if (debtLoanType == DebtLoanType.MORTGAGE_LOAN) {
                if (loanType == LoanManageAccount.LOAN_TYPE_ONCE) {
                    getMortgageLoanTypeAmount(this)
                } else {
                    loanCalcResult.sumBy { it.principal }.toDouble()
                }
            } else 0.0
        }
    }

    private fun getCalcDsrRepayAmount(debtCalcAccount: DebtCalcAccount, loanCalcResult: List<CalcResult>): Double {
        debtCalcAccount.apply {
            return when (debtLoanType) {
                DebtLoanType.MORTGAGE_LOAN -> {
                    if (loanType == LoanManageAccount.LOAN_TYPE_ONCE) {
                        getMortgageLoanTypeAmount(this)
                    } else {
                        loanCalcResult.sumBy { it.principal }.toDouble()
                    }
                }
                DebtLoanType.MIDDLE_PAYMENT_LOAN, DebtLoanType.MOVING_LOAN -> (loanAmount.div(25)).toDouble()
                DebtLoanType.RENT_LOAN -> 0.0
                DebtLoanType.RENT_DEPOSIT_BASED_LOAN -> (loanAmount.div(4)).toDouble()
                DebtLoanType.PERSONAL_LOAN -> (loanAmount.div(10)).toDouble()
                DebtLoanType.DEPOSIT_BASED_LOAN, DebtLoanType.STOCK_BASED_LOAN -> (loanAmount.div(8)).toDouble()
                else -> {
                    if (loanType == LoanManageAccount.LOAN_TYPE_ONCE) {
                        getEtcLoanTypeYearlyAmount(this)
                    } else {
                        loanCalcResult.sumBy { it.principal }.toDouble()
                    }
                }

            }.roundToInt().toDouble()
        }
    }

    private fun calcDti(debtCalcAccounts: List<DebtCalcAccount>?, yearlyIncome: Long): Double {
        val dtiTotalRepay = calcDtiTotalRepay(debtCalcAccounts)?.toDouble()
        return dtiTotalRepay?.let { it * 100 / yearlyIncome.toDouble() } ?: 0.0
    }

    private fun calcDtiYearlyRepay(debtCalcAccounts: List<DebtCalcAccount>?): Long? {
        return debtCalcAccounts?.map { it.dtiTotalRepayAmount.toLong() }?.sum()
    }

    private fun calcDtiTotalRepay(debtCalcAccounts: List<DebtCalcAccount>?): Long? {
        return debtCalcAccounts?.map { it.dtiTotalRepayAmount.toLong() }?.sum()
    }

    private fun calcDsr(debtCalcAccounts: List<DebtCalcAccount>?, yearlyIncome: Long): Double {
        val dsrTotalRepay = calcDsrTotalRepay(debtCalcAccounts)?.toDouble()
        return dsrTotalRepay?.let { it * 100 / yearlyIncome.toDouble() } ?: 0.0
    }

    private fun calcDsrYearlyRepay(debtCalcAccounts: List<DebtCalcAccount>?): Long? {
        return debtCalcAccounts?.map { it.dsrTotalRepayAmount.toLong() }?.sum()
    }

    private fun calcDsrTotalRepay(debtCalcAccounts: List<DebtCalcAccount>?): Long? {
        return debtCalcAccounts?.map { it.dsrTotalRepayAmount.toLong() }?.sum()
    }

    /**
     * @author 홍성하
     * @link https://m.blog.naver.com/altwellaaha/221201936003
     * 만기일시의 경우에 계산하는 DTI, DSR의 식은 다름
     * 만기일시는 마지막 달만 원금을 상환하고 다른 기간에는 이자만 균등분할상환하기 때문에
     * 일부 경우에서 원리금, 원금상환과 동일하게 현재에서 12개월(1년)을 계산하는 경우 이자만 도출된다.
     * 따라서 만기일시상환의 경우 계산하는 방식은
     * 매달 이자 x 12(1년) + 원금총액 / 총 대출기간(최대 10년) 로 계산합니다.
     */
    // 주택담보대출 만기일시
    private fun getMortgageLoanTypeAmount(debtCalcAccount: DebtCalcAccount): Double {
        debtCalcAccount.apply {
            if (yearlyRate == 0.0) return 0.0
            val targetPeriod = if (remainMonth < 120) remainMonth.toDouble() else 120.toDouble()
            return loanAmount / (targetPeriod / 12)
        }
    }

    // 1년 동안 만기일시 원금
    private fun getEtcLoanTypeYearlyAmount(debtCalcAccount: DebtCalcAccount): Double {
        debtCalcAccount.apply {
            if (yearlyRate == 0.0) return 0.0
            return if (remainMonth > 12) 0.0 else loanAmount.toDouble()
        }
    }
}