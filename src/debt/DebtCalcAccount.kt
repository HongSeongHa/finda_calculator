package debt

import data.LoanManageAccount
import model.calc.CalcAccount
import model.calc.DebtLoanType
import java.math.BigDecimal
import java.util.*

class DebtCalcAccount : CalcAccount {
    var accountId: String
        private set
    var bankName: String? = null
    var isCustomAccount: Boolean
        private set
    var debtLoanType: DebtLoanType? = null

    var yearlyPrinciple = 0.0
    var yearlyInterest = 0.0

    var dtiRepayAmount = 0.0
    var dsrRepayAmount = 0.0

    // DTI 상세 계산 내역 금액
    val dtiTotalRepayAmount: Double
        get() = dtiRepayAmount + yearlyInterest

    // DSR 상세 계산 내역 금액
    val dsrTotalRepayAmount: Double
        get() = dsrRepayAmount + yearlyInterest

    // 가상 계좌로 생성
    constructor() {
        accountId = UUID.randomUUID().toString()
        bankName = "추가대출"
        isCustomAccount = true
    }

    /**
     * 실제 데이터 계좌로 생성
     * 이와 같은 방식으로 생성하는 경우에도 loanType은 지정해서 사용해야 합니다.
     */
    constructor(loanManageAccount: LoanManageAccount) {
        accountId = loanManageAccount.id.toString()
        bankName = loanManageAccount.bankName

        loanAmount = loanManageAccount.balanceAmount ?: 0
        yearlyRate = loanManageAccount.loanRate ?: 0.0

        remainMonth = loanManageAccount.remainRepayPeriod
        unredeemedPeriod = loanManageAccount.unredeemedPeriod ?: 0

        loanType = loanManageAccount.loanType
        openDate = loanManageAccount.openDate ?: 0
        dueDate = loanManageAccount.dueDate ?: 0
        repayDay = loanManageAccount.repayDay ?: 0

        isCustomAccount = false
    }

    override fun toString(): String {
        return """
            loanType : $debtLoanType
            yearlyPrinciple : ${BigDecimal(yearlyPrinciple)}
            yearlyInterest : ${BigDecimal(yearlyInterest)}
            
            dtiRepayAmount : ${BigDecimal(dtiRepayAmount)}
            dsrRepayAmount : ${BigDecimal(dsrRepayAmount)}
           
            dtiTotalRepayAmount : ${BigDecimal(dtiTotalRepayAmount)}
            dsrTotalRepayAmount : ${BigDecimal(dsrTotalRepayAmount)}


        """.trimIndent()
    }
}