import com.google.gson.Gson
import data.LoanManageAccount

const val jsonString = """[
  {
    "agreeAmount": 22000000,
    "balanceAmount": 22000000,
    "bank": {
      "cooconModule": "knbank",
      "id": 15
    },
    "bankName": "BNK경남은행",
    "checkBalanceAmount": 22000000,
    "checkMonth": 202009,
    "checkRepayAmount": 70000,
    "dueDate": 1614265200000,
    "id": 149,
    "insertTime": 1582853177000,
    "isEnable": true,
    "loanRate": 0.0374633,
    "loanType": "일시상환",
    "loanTypeI": "일시상환",
    "loanTypeK": "일시상환",
    "monthlyRepayList": [
      {
        "interest": 67741,
        "month": 1601478000000,
        "principal": 0,
        "repayDate": 1603638000000
      },
      {
        "interest": 69999,
        "month": 1604156400000,
        "principal": 0,
        "repayDate": 1606316400000
      },
      {
        "interest": 67741,
        "month": 1606748400000,
        "principal": 0,
        "repayDate": 1609081200000
      },
      {
        "interest": 69999,
        "month": 1609426800000,
        "principal": 0,
        "repayDate": 1611586800000
      },
      {
        "interest": 69999,
        "month": 1612105200000,
        "principal": 22000000,
        "repayDate": 1614265200000
      }
    ],
    "openDate": 1582642800000,
    "repayDay": 26,
    "repayDayI": 26,
    "unredeemedPeriod": 0,
    "unredeemedPeriodI": 0,
    "updateTime": 1602731136000
  },
  {
    "agreeAmount": 30000000,
    "balanceAmount": 27637000,
    "bank": {
      "cooconModule": "knbank",
      "id": 15
    },
    "bankName": "BNK경남은행",
    "checkBalanceAmount": 27637000,
    "checkMonth": 202009,
    "checkRepayAmount": 886000,
    "dueDate": 1687359600000,
    "id": 1359,
    "insertTime": 1592962098000,
    "isEnable": true,
    "loanRate": 0.0408,
    "loanRateI": 0.0408,
    "loanType": "원금균등상환",
    "loanTypeI": "원금균등상환",
    "loanTypeK": "분할상환",
    "monthlyRepayList": [
      {
        "interest": 93965,
        "month": 1601478000000,
        "principal": 837484,
        "repayDate": 1603292400000
      },
      {
        "interest": 91118,
        "month": 1604156400000,
        "principal": 837484,
        "repayDate": 1606057200000
      },
      {
        "interest": 88270,
        "month": 1606748400000,
        "principal": 837484,
        "repayDate": 1608562800000
      },
      {
        "interest": 85423,
        "month": 1609426800000,
        "principal": 837484,
        "repayDate": 1611241200000
      },
      {
        "interest": 82576,
        "month": 1612105200000,
        "principal": 837484,
        "repayDate": 1613919600000
      },
      {
        "interest": 79728,
        "month": 1614524400000,
        "principal": 837485,
        "repayDate": 1616338800000
      },
      {
        "interest": 76881,
        "month": 1617202800000,
        "principal": 837485,
        "repayDate": 1619017200000
      },
      {
        "interest": 74033,
        "month": 1619794800000,
        "principal": 837485,
        "repayDate": 1621782000000
      },
      {
        "interest": 71186,
        "month": 1622473200000,
        "principal": 837485,
        "repayDate": 1624287600000
      },
      {
        "interest": 68338,
        "month": 1625065200000,
        "principal": 837485,
        "repayDate": 1626879600000
      },
      {
        "interest": 65491,
        "month": 1627743600000,
        "principal": 837485,
        "repayDate": 1629644400000
      },
      {
        "interest": 62643,
        "month": 1630422000000,
        "principal": 837485,
        "repayDate": 1632322800000
      },
      {
        "interest": 59796,
        "month": 1633014000000,
        "principal": 837485,
        "repayDate": 1634828400000
      },
      {
        "interest": 56948,
        "month": 1635692400000,
        "principal": 837485,
        "repayDate": 1637506800000
      },
      {
        "interest": 54101,
        "month": 1638284400000,
        "principal": 837485,
        "repayDate": 1640098800000
      },
      {
        "interest": 51254,
        "month": 1640962800000,
        "principal": 837485,
        "repayDate": 1642950000000
      },
      {
        "interest": 48406,
        "month": 1643641200000,
        "principal": 837485,
        "repayDate": 1645455600000
      },
      {
        "interest": 45559,
        "month": 1646060400000,
        "principal": 837485,
        "repayDate": 1647874800000
      },
      {
        "interest": 42711,
        "month": 1648738800000,
        "principal": 837485,
        "repayDate": 1650553200000
      },
      {
        "interest": 39864,
        "month": 1651330800000,
        "principal": 837485,
        "repayDate": 1653231600000
      },
      {
        "interest": 37016,
        "month": 1654009200000,
        "principal": 837485,
        "repayDate": 1655823600000
      },
      {
        "interest": 34169,
        "month": 1656601200000,
        "principal": 837485,
        "repayDate": 1658415600000
      },
      {
        "interest": 31321,
        "month": 1659279600000,
        "principal": 837485,
        "repayDate": 1661094000000
      },
      {
        "interest": 28474,
        "month": 1661958000000,
        "principal": 837485,
        "repayDate": 1663772400000
      },
      {
        "interest": 25627,
        "month": 1664550000000,
        "principal": 837485,
        "repayDate": 1666537200000
      },
      {
        "interest": 22779,
        "month": 1667228400000,
        "principal": 837485,
        "repayDate": 1669042800000
      },
      {
        "interest": 19932,
        "month": 1669820400000,
        "principal": 837485,
        "repayDate": 1671634800000
      },
      {
        "interest": 17084,
        "month": 1672498800000,
        "principal": 837485,
        "repayDate": 1674486000000
      },
      {
        "interest": 14237,
        "month": 1675177200000,
        "principal": 837485,
        "repayDate": 1676991600000
      },
      {
        "interest": 11389,
        "month": 1677596400000,
        "principal": 837485,
        "repayDate": 1679410800000
      },
      {
        "interest": 8542,
        "month": 1680274800000,
        "principal": 837485,
        "repayDate": 1682262000000
      },
      {
        "interest": 5694,
        "month": 1682866800000,
        "principal": 837485,
        "repayDate": 1684681200000
      },
      {
        "interest": 2847,
        "month": 1685545200000,
        "principal": 837485,
        "repayDate": 1687359600000
      }
    ],
    "openDate": 1592751600000,
    "repayDay": 22,
    "repayDayI": 22,
    "unredeemedPeriod": 0,
    "unredeemedPeriodI": 0,
    "updateTime": 1602731136000
  },
  {
    "accountNumber": 7010904006341,
    "agreeAmount": 118000000,
    "balanceAmount": 61788000,
    "bank": {
      "cooconModule": "kbstar",
      "id": 2
    },
    "bankName": "국민은행",
    "checkBalanceAmount": 61788000,
    "checkMonth": 202009,
    "checkRepayAmount": 1538000,
    "dueDate": 1711810800000,
    "id": 11,
    "insertTime": 1573639754000,
    "isEnable": true,
    "loanRate": 0.025,
    "loanRateI": 0.025,
    "loanType": "원리금균등상환",
    "loanTypeI": "원리금균등상환",
    "loanTypeK": "분할상환",
    "monthlyRepayList": [
      {
        "interest": 128725,
        "month": 1601478000000,
        "principal": 1409249,
        "repayDate": 1603638000000
      },
      {
        "interest": 125789,
        "month": 1604156400000,
        "principal": 1412185,
        "repayDate": 1606230000000
      },
      {
        "interest": 122847,
        "month": 1606748400000,
        "principal": 1415127,
        "repayDate": 1609081200000
      },
      {
        "interest": 119898,
        "month": 1609426800000,
        "principal": 1418076,
        "repayDate": 1611500400000
      },
      {
        "interest": 116944,
        "month": 1612105200000,
        "principal": 1421030,
        "repayDate": 1614178800000
      },
      {
        "interest": 113984,
        "month": 1614524400000,
        "principal": 1423990,
        "repayDate": 1616598000000
      },
      {
        "interest": 111017,
        "month": 1617202800000,
        "principal": 1426957,
        "repayDate": 1619362800000
      },
      {
        "interest": 108044,
        "month": 1619794800000,
        "principal": 1429930,
        "repayDate": 1621868400000
      },
      {
        "interest": 105065,
        "month": 1622473200000,
        "principal": 1432909,
        "repayDate": 1624546800000
      },
      {
        "interest": 102080,
        "month": 1625065200000,
        "principal": 1435894,
        "repayDate": 1627225200000
      },
      {
        "interest": 99088,
        "month": 1627743600000,
        "principal": 1438886,
        "repayDate": 1629817200000
      },
      {
        "interest": 96091,
        "month": 1630422000000,
        "principal": 1441883,
        "repayDate": 1632668400000
      },
      {
        "interest": 93087,
        "month": 1633014000000,
        "principal": 1444887,
        "repayDate": 1635087600000
      },
      {
        "interest": 90077,
        "month": 1635692400000,
        "principal": 1447897,
        "repayDate": 1637766000000
      },
      {
        "interest": 87060,
        "month": 1638284400000,
        "principal": 1450915,
        "repayDate": 1640530800000
      },
      {
        "interest": 84037,
        "month": 1640962800000,
        "principal": 1453938,
        "repayDate": 1643036400000
      },
      {
        "interest": 81008,
        "month": 1643641200000,
        "principal": 1456966,
        "repayDate": 1645714800000
      },
      {
        "interest": 77973,
        "month": 1646060400000,
        "principal": 1460001,
        "repayDate": 1648134000000
      },
      {
        "interest": 74931,
        "month": 1648738800000,
        "principal": 1463044,
        "repayDate": 1650812400000
      },
      {
        "interest": 71883,
        "month": 1651330800000,
        "principal": 1466091,
        "repayDate": 1653404400000
      },
      {
        "interest": 68829,
        "month": 1654009200000,
        "principal": 1469145,
        "repayDate": 1656255600000
      },
      {
        "interest": 65768,
        "month": 1656601200000,
        "principal": 1472206,
        "repayDate": 1658674800000
      },
      {
        "interest": 62701,
        "month": 1659279600000,
        "principal": 1475274,
        "repayDate": 1661353200000
      },
      {
        "interest": 59628,
        "month": 1661958000000,
        "principal": 1478346,
        "repayDate": 1664118000000
      },
      {
        "interest": 56548,
        "month": 1664550000000,
        "principal": 1481427,
        "repayDate": 1666623600000
      },
      {
        "interest": 53461,
        "month": 1667228400000,
        "principal": 1484514,
        "repayDate": 1669302000000
      },
      {
        "interest": 50369,
        "month": 1669820400000,
        "principal": 1487605,
        "repayDate": 1671980400000
      },
      {
        "interest": 47270,
        "month": 1672498800000,
        "principal": 1490704,
        "repayDate": 1674572400000
      },
      {
        "interest": 44164,
        "month": 1675177200000,
        "principal": 1493811,
        "repayDate": 1677423600000
      },
      {
        "interest": 41052,
        "month": 1677596400000,
        "principal": 1496923,
        "repayDate": 1679842800000
      },
      {
        "interest": 37933,
        "month": 1680274800000,
        "principal": 1500042,
        "repayDate": 1682348400000
      },
      {
        "interest": 34808,
        "month": 1682866800000,
        "principal": 1503166,
        "repayDate": 1684940400000
      },
      {
        "interest": 31677,
        "month": 1685545200000,
        "principal": 1506297,
        "repayDate": 1687705200000
      },
      {
        "interest": 28538,
        "month": 1688137200000,
        "principal": 1509437,
        "repayDate": 1690210800000
      },
      {
        "interest": 25394,
        "month": 1690815600000,
        "principal": 1512580,
        "repayDate": 1692889200000
      },
      {
        "interest": 22243,
        "month": 1693494000000,
        "principal": 1515732,
        "repayDate": 1695567600000
      },
      {
        "interest": 19085,
        "month": 1696086000000,
        "principal": 1518890,
        "repayDate": 1698159600000
      },
      {
        "interest": 15920,
        "month": 1698764400000,
        "principal": 1522055,
        "repayDate": 1701010800000
      },
      {
        "interest": 12749,
        "month": 1701356400000,
        "principal": 1525225,
        "repayDate": 1703516400000
      },
      {
        "interest": 9572,
        "month": 1704034800000,
        "principal": 1528402,
        "repayDate": 1706108400000
      },
      {
        "interest": 6388,
        "month": 1706713200000,
        "principal": 1531586,
        "repayDate": 1708873200000
      },
      {
        "interest": 3197,
        "month": 1709218800000,
        "principal": 1534778,
        "repayDate": 1711292400000
      }
    ],
    "openDate": 1396191600000,
    "productName": "KB주택전세자금대출",
    "repayDay": 25,
    "repayDayI": 25,
    "repayDayS": 25,
    "unredeemedPeriod": 36,
    "unredeemedPeriodI": 36,
    "updateTime": 1602731136000
  },
  {
    "agreeAmount": 5700000,
    "balanceAmount": 5700000,
    "bank": {
      "cooconModule": "shinhan",
      "id": 4
    },
    "bankName": "신한은행",
    "checkBalanceAmount": 5700000,
    "checkMonth": 202009,
    "checkRepayAmount": 9000,
    "dueDate": 1608908400000,
    "id": 10,
    "insertTime": 1573639754000,
    "isEnable": true,
    "loanRate": 0.0269,
    "loanRateI": 0.0269,
    "loanType": "일시상환",
    "loanTypeI": "일시상환",
    "loanTypeK": "일시상환",
    "monthlyRepayList": [
      {
        "interest": 12602,
        "month": 1601478000000,
        "principal": 0,
        "repayDate": 1603638000000
      },
      {
        "interest": 13022,
        "month": 1604156400000,
        "principal": 0,
        "repayDate": 1606316400000
      },
      {
        "interest": 12602,
        "month": 1606748400000,
        "principal": 5700000,
        "repayDate": 1609081200000
      }
    ],
    "openDate": 1545750000000,
    "repayDay": 26,
    "repayDayI": 26,
    "unredeemedPeriod": 0,
    "unredeemedPeriodI": 0,
    "updateTime": 1602731136000
  },
  {
    "agreeAmount": 38000000,
    "balanceAmount": 20058000,
    "bankName": "카카오뱅크",
    "checkBalanceAmount": 20058000,
    "checkMonth": 202009,
    "checkRepayAmount": 0,
    "dueDate": 1630767600000,
    "id": 12,
    "insertTime": 1573639754000,
    "isEnable": true,
    "loanRate": 0.036,
    "loanRateI": 0.036,
    "loanType": "한도대출",
    "loanTypeK": "한도대출",
    "monthlyRepayList": [],
    "openDate": 1567609200000,
    "repayDay": 5,
    "repayDayI": 5,
    "unredeemedPeriod": 0,
    "unredeemedPeriodI": 0,
    "updateTime": 1602731136000
  }
]
"""

/**
 * 홍민님의 실제 대출 데이터를 jsonString으로 변경해서 저장하고, get하는 방법으로 사용.
 * 분리한 이유는 jsonString이 매우 길었기 때문.
 */
fun getLoanManageAccountObjectExample(): Array<LoanManageAccount> {
    return Gson().fromJson(jsonString, Array<LoanManageAccount>::class.java)
}