import advance.AdvanceCalcAccount
import advance.AdvanceCalcResult
import data.LoanManageAccount
import debt.DebtCalcAccount
import debt.DebtCalculator
import ext.excludeFilterByLoanType
import model.calc.CalcAccount
import model.calc.DebtLoanType
import util.CalculatorUtil

fun main() {

    /**
     * CalcAccount의 선언부에 대한 자세한 설명은 클래스 주석을 참고하세요.
     * @see CalcAccount
     *
     * 나온 결과값을 사용할 때는 주로 sumByLong 확장 함수를 사용합니다. sumBy와 동일한 역할을 합니다.
     * @see List.sumBy
     */
//    val calcAccount = CalcAccount().apply {
//        loanType = LoanManageAccount.LOAN_TYPE_EQUAL_PAY
//        loanAmount = 20_000_000
//        yearlyRate = 0.045
//        remainMonth = 24
//        unredeemedPeriod = 0
//    }
//
//    CalculatorUtil(calcAccount).calculate().forEach {
//        println(it)
//    }

    /**
     * DIT, DSR 도출 부분입니다.
     * 사용법은 아래와 같습니다.
     * 1. 각 DebtCalcAccount의 대출 방식(debtLoanType)을 필수적으로 정해줍니다.
     * 2. 계산하고자 하는 모든 대출 account를 하나의 리스트로 만듭니다.
     * 3. DebtCalculator()에 인자로 2번의 list와 연 소득 총액(단위 : 원)을 기입하고 도출된 Result를 사용합니다.
     */
//    val list = getLoanManageAccountObjectExample().asList()
//
//    val debtAccount1 = DebtCalcAccount(list[0]).apply { debtLoanType = DebtLoanType.??? }
//    val debtAccount2 = DebtCalcAccount(list[1]).apply { debtLoanType = DebtLoanType.??? }
//    val debtAccount3 = DebtCalcAccount(list[2]).apply { debtLoanType = DebtLoanType.??? }
//    val debtAccount4 = DebtCalcAccount(list[3]).apply { debtLoanType = DebtLoanType.??? }
//
//    val debtList = listOf(debtAccount1, debtAccount2, debtAccount3, debtAccount4)
//
//    val debtResult = DebtCalculator(debtList, 100_000_000L).calculate()
//
//    debtResult.dti
//    debtResult.dsr
//    debtResult.???

    /**
     * 여윳돈 계산기 부분 로직입니다.
     * 사용법은 아래와 같습니다.
     * `AdvanceCalcResult(LoanManageAccount List(유저 대출 리스트), 여윳돈)`
     *
     * excludeFilterByLoanType() 확장 함수를 사용하는 이유?
     * 여윳돈을 계산할 때에는 마이너스 통장을 제외하고 계산함.
     */
//    val advanceCalcResult = AdvanceCalcResult(getLoanManageAccountObjectExample().asList().excludeFilterByLoanType(), 50_000_000L)

    val advanceCalcAccounts = getLoanManageAccountObjectExample().asList().excludeFilterByLoanType()


    advanceCalcAccounts.forEach {
        println(AdvanceCalcAccount(it))
    }
}