package advance

import data.LoanManageAccount
import ext.sumBy
import util.CalculatorUtil
import util.RemainUnredeemedUtil


class AdvanceCalculator(private val advanceCalcAccount: AdvanceCalcAccount) {
    fun calculate() {
        advanceCalcAccount.apply {
            val copyAccount = this.clone().also { it.loanAmount -= it.expectedRepayAmount }

            // 이자
            interestBeforeRepay = getInterest(this)
            interestAfterRepay = getInterest(copyAccount)
            interestDiff = interestBeforeRepay - interestAfterRepay

            // 월 부담금
            monthlyAmountBeforeRepay = getRepayAmount(this)
            monthlyAmountAfterRepay = getRepayAmount(copyAccount)
            monthlyAmountDiff = monthlyAmountBeforeRepay - monthlyAmountAfterRepay
        }
    }


    // 여윳돈 사용 시 감소 이자 & 감소 부담액 계산
    private fun getInterest(account: AdvanceCalcAccount): Double {
        account.apply {
            return when (loanType) {
                LoanManageAccount.LOAN_TYPE_EQUAL_ORIGIN, LoanManageAccount.LOAN_TYPE_EQUAL_PAY -> {
                    val remainUnredeemedPeriod =
                        RemainUnredeemedUtil.getRemainUnredeemedPeriod(this, true)

                    this.unredeemedPeriod = remainUnredeemedPeriod.toInt()

                    val result = CalculatorUtil(this).calculate()

                    result.sumBy { it.interest }.toDouble()
                }
                else -> getEtcLoanTypeInterest(loanAmount)
            }
        }
    }

    private fun getRepayAmount(account: AdvanceCalcAccount): Double {
        account.apply {
            return when (loanType) {
                LoanManageAccount.LOAN_TYPE_EQUAL_ORIGIN, LoanManageAccount.LOAN_TYPE_EQUAL_PAY -> {
                    val remainUnredeemedPeriod =
                        RemainUnredeemedUtil.getRemainUnredeemedPeriod(this, true)

                    this.unredeemedPeriod = remainUnredeemedPeriod.toInt()

                    CalculatorUtil(this).calculate()[0].total.toDouble()
                }
                else -> getEtcLoanTypeMonthlyAmount(loanAmount)
            }
        }
    }

    /**
     * 기타(만기일시, 마이너스)
     */
    private fun getEtcLoanTypeMonthlyAmount(loanAmount: Long): Double {
        advanceCalcAccount.apply {
            val monthlyLoanRate = yearlyRate / 12
            return loanAmount * monthlyLoanRate
        }
    }

    private fun getEtcLoanTypeInterest(loanAmount: Long): Double {
        advanceCalcAccount.apply {
            val monthlyLoanRate = yearlyRate / 12
            return remainMonth * loanAmount * monthlyLoanRate
        }
    }
}