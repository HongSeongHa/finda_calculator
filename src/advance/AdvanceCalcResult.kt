package advance

import data.LoanManageAccount

class AdvanceCalcResult(
    loanManageAccountList: List<LoanManageAccount>?,
    var spareMoney: Long
) {

    var interestCalcAccountList: MutableList<AdvanceCalcAccount>? =
        null
        private set
    var monthlyAmountCalcAccountList: MutableList<AdvanceCalcAccount>? =
        null
        private set

    // 이자 줄이기
    var totalInterestBeforeRepay: Long = 0
        private set
    var totalInterestAfterRepay: Long = 0
        private set
    var totalBeforeInterestForInterestSaving: Long = 0
        private set
    var totalAfterInterestForInterestSaving: Long = 0
        private set
    var totalInterestDiffForInterestSaving: Long = 0
        private set
    var totalBeforeMonthlyAmountForInterestSaving: Long = 0
        private set
    var totalAfterMonthlyAmountForInterestSaving: Long = 0
        private set
    var totalMonthlyAmountDiffForInterestSaving: Long = 0
        private set

    // 월부담액 줄이기
    var totalMonthlyRepayBeforeRepay: Long = 0
        private set
    var totalMonthlyRepayAfterRepay: Long = 0
        private set
    var totalBeforeInterestForMonthlyAmountSaving: Long = 0
        private set
    var totalAfterInterestForMonthlyAmountSaving: Long = 0
        private set
    var totalInterestDiffForMonthlyAmountSaving: Long = 0
        private set
    var totalBeforeMonthlyAmountForMonthlyAmountSaving: Long = 0
        private set
    var totalAfterMonthlyAmountForMonthlyAmountSaving: Long = 0
        private set
    var totalMonthlyAmountDiffForMonthlyAmountSaving: Long = 0
        private set

    var calculatedTime: Long = System.currentTimeMillis()

    // 잔액
    var totalBalanceAmount: Long

    // 상환금
    var totalExpectedRepayAmount: Long = 0
        private set

    val totalSavingInterest: Long
        get() = totalInterestBeforeRepay - totalInterestAfterRepay

    val totalSavingMonthlyRepay: Long
        get() = totalMonthlyRepayBeforeRepay - totalMonthlyRepayAfterRepay

    private fun makeCalculateAccountList(loanManageAccountList: List<LoanManageAccount>?): List<AdvanceCalcAccount>? =
        loanManageAccountList?.map { AdvanceCalcAccount(it) }

    /**
     * Constructor
     */
    init { // 레퍼런스가 꼬일수 있으므로 makeCalculateAccountList 메서드로 각각 다른 list를 생성해야함
        val haveLockedAccount = loanManageAccountList?.any { it.isAccountLocked } ?: true

        if (!haveLockedAccount) {
            setInterestCalculateAccountList(
                makeCalculateAccountList(loanManageAccountList),
                spareMoney
            )
            setMonthlyAmountCalculateAccountList(
                makeCalculateAccountList(loanManageAccountList),
                spareMoney
            )
        }

        totalBalanceAmount = 0

        loanManageAccountList?.forEach { account ->
            account.balanceAmount?.let { totalBalanceAmount += it }
        }
    }

    /**
     * 총 줄어드는 이자 비용 기준으로 세팅된 calculateAccountList 생성
     */
    private fun setInterestCalculateAccountList(
        advanceCalcAccountList: List<AdvanceCalcAccount>?,
        spareMoney: Long
    ) {
        var spareMoney = spareMoney
        interestCalcAccountList = mutableListOf()
        advanceCalcAccountList?.let {
            val sortedAccountList =
                it.sortedWith(Comparator { o1, o2 -> if (o1.interestDiff - o2.interestDiff > 0) -1 else 1 })
                    .toMutableList()
            interestCalcAccountList?.addAll(sortedAccountList)
        }

        initTotalInterestVariable()

        interestCalcAccountList?.forEach {
            if (spareMoney < 0) {
                it.expectedRepayAmount = 0
            } else {
                val loanBalance = it.loanAmount
                val expectedRepayAmount =
                    if (spareMoney > loanBalance) loanBalance else spareMoney
                it.expectedRepayAmount = expectedRepayAmount
                spareMoney -= expectedRepayAmount

                totalInterestBeforeRepay += it.interestBeforeRepay.toInt()
                totalInterestAfterRepay += it.interestAfterRepay.toInt()
                totalExpectedRepayAmount += expectedRepayAmount

                totalBeforeInterestForInterestSaving += it.interestBeforeRepay.toInt()
                totalAfterInterestForInterestSaving += it.interestAfterRepay.toInt()
                totalInterestDiffForInterestSaving += it.interestDiff.toInt()

                totalBeforeMonthlyAmountForInterestSaving += it.monthlyAmountBeforeRepay.toInt()
                totalAfterMonthlyAmountForInterestSaving += it.monthlyAmountAfterRepay.toInt()
                totalMonthlyAmountDiffForInterestSaving += it.monthlyAmountDiff.toInt()
            }
        }
    }

    /**
     * 총 줄어드는 월 부담금 기준으로 세팅된 calculateAccountList 생성
     */
    private fun setMonthlyAmountCalculateAccountList(
        advanceCalcAccountList: List<AdvanceCalcAccount>?,
        spareMoney: Long
    ) {
        var spareMoney = spareMoney
        monthlyAmountCalcAccountList = mutableListOf()

        advanceCalcAccountList?.let {
            val sortedAccountList =
                it.sortedWith(Comparator { o1: AdvanceCalcAccount, o2: AdvanceCalcAccount ->
                    if (o1.monthlyAmountDiff - o2.monthlyAmountDiff > 0) -1 else 1
                }).toMutableList()
            monthlyAmountCalcAccountList?.addAll(sortedAccountList)
        }

        initTotalMonthlyAmountVariable()

        monthlyAmountCalcAccountList?.forEach {
            if (spareMoney < 0) {
                it.expectedRepayAmount = 0
            } else {
                val loanBalance = it.loanAmount
                val expectedRepayAmount = if (spareMoney > loanBalance) loanBalance else spareMoney
                it.expectedRepayAmount = expectedRepayAmount
                spareMoney -= expectedRepayAmount

                totalMonthlyRepayBeforeRepay += it.monthlyAmountBeforeRepay.toInt()
                totalMonthlyRepayAfterRepay += it.monthlyAmountAfterRepay.toInt()
                totalExpectedRepayAmount += expectedRepayAmount

                totalBeforeInterestForMonthlyAmountSaving += it.interestBeforeRepay.toInt()
                totalAfterInterestForMonthlyAmountSaving += it.interestAfterRepay.toInt()
                totalInterestDiffForMonthlyAmountSaving += it.interestDiff.toInt()

                totalBeforeMonthlyAmountForMonthlyAmountSaving += it.monthlyAmountBeforeRepay.toInt()
                totalAfterMonthlyAmountForMonthlyAmountSaving += it.monthlyAmountAfterRepay.toInt()
                totalMonthlyAmountDiffForMonthlyAmountSaving += it.monthlyAmountDiff.toInt()
            }
        }
    }


    /**
     * setInterestCalculateAccountList() 에서 사용하는 총합 변수들을 초기화하는 함수
     */
    private fun initTotalInterestVariable() {
        totalInterestBeforeRepay = 0
        totalInterestAfterRepay = 0
        totalExpectedRepayAmount = 0
        totalBeforeInterestForInterestSaving = 0
        totalAfterInterestForInterestSaving = 0
        totalInterestDiffForInterestSaving = 0
        totalBeforeMonthlyAmountForInterestSaving = 0
        totalAfterMonthlyAmountForInterestSaving = 0
        totalMonthlyAmountDiffForInterestSaving = 0
    }

    /**
     * setMonthlyAmountCalculateAccountList() 에서 사용하는 총합 변수 변수들을 초기화하는 함수
     */
    private fun initTotalMonthlyAmountVariable() {
        totalMonthlyRepayBeforeRepay = 0
        totalMonthlyRepayAfterRepay = 0
        totalExpectedRepayAmount = 0
        totalBeforeInterestForMonthlyAmountSaving = 0
        totalAfterInterestForMonthlyAmountSaving = 0
        totalInterestDiffForMonthlyAmountSaving = 0
        totalBeforeMonthlyAmountForMonthlyAmountSaving = 0
        totalAfterMonthlyAmountForMonthlyAmountSaving = 0
        totalMonthlyAmountDiffForMonthlyAmountSaving = 0
    }

    override fun toString(): String {
        return """
            
            이자 줄이기 계좌 리스트
            $interestCalcAccountList
            
            월부담금 줄이기 리스트
            $monthlyAmountCalcAccountList
                                                           
            totalInterestBeforeRepay                       : $totalInterestBeforeRepay
            totalInterestAfterRepay                        : $totalInterestAfterRepay
            totalBeforeInterestForInterestSaving           : $totalBeforeInterestForInterestSaving
            totalAfterInterestForInterestSaving            : $totalAfterInterestForInterestSaving
            totalInterestDiffForInterestSaving             : $totalInterestDiffForInterestSaving
            totalBeforeMonthlyAmountForInterestSaving      : $totalBeforeMonthlyAmountForInterestSaving
            totalAfterMonthlyAmountForInterestSaving       : $totalAfterMonthlyAmountForInterestSaving
            totalMonthlyAmountDiffForInterestSaving        : $totalMonthlyAmountDiffForInterestSaving
                                                           : 
            totalMonthlyRepayBeforeRepay                   : $totalMonthlyRepayBeforeRepay
            totalMonthlyRepayAfterRepay                    : $totalMonthlyRepayAfterRepay
            totalBeforeInterestForMonthlyAmountSaving      : $totalBeforeInterestForMonthlyAmountSaving
            totalAfterInterestForMonthlyAmountSaving       : $totalAfterInterestForMonthlyAmountSaving
            totalInterestDiffForMonthlyAmountSaving        : $totalInterestDiffForMonthlyAmountSaving
            totalBeforeMonthlyAmountForMonthlyAmountSaving : $totalBeforeMonthlyAmountForMonthlyAmountSaving
            totalAfterMonthlyAmountForMonthlyAmountSaving  : $totalAfterMonthlyAmountForMonthlyAmountSaving
            totalMonthlyAmountDiffForMonthlyAmountSaving   : $totalMonthlyAmountDiffForMonthlyAmountSaving
                                                           : 
            totalBalanceAmount                             : $totalBalanceAmount
            totalExpectedRepayAmount                       : $totalExpectedRepayAmount
            totalSavingInterest                            : $totalSavingInterest
            totalSavingMonthlyRepay                        : $totalSavingMonthlyRepay
        """.trimIndent()
    }
}