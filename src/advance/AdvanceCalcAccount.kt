package advance

import data.LoanManageAccount
import model.calc.CalcAccount


class AdvanceCalcAccount(loanManageAccount: LoanManageAccount) : CalcAccount(), Cloneable {

    // 은행명
    val bankName: String? = loanManageAccount.bankName
    // 상환전 이자
    var interestBeforeRepay = 0.0
    // 상환후 이자
    var interestAfterRepay = 0.0
    // 총 출어드는 이자 비용 = 0.0
    var interestDiff = 0.0
    // 상환전 월 부담액 = 0.0
    var monthlyAmountBeforeRepay = 0.0
    // 상환후 월 부담액 = 0.0
    var monthlyAmountAfterRepay = 0.0
    // 매월 줄어드는 부담액 = 0.0
    var monthlyAmountDiff = 0.0
    // 여유자금 중 상환 금액
    var expectedRepayAmount: Long = 0
        set(value) {
            field = value
            AdvanceCalculator(this).calculate()
        }

    public override fun clone(): AdvanceCalcAccount {
        return super.clone() as AdvanceCalcAccount
    }

    /**
     * Constructor
     */
    init {
        loanType = loanManageAccount.loanType
        loanAmount = loanManageAccount.balanceAmount ?: 0
        yearlyRate = loanManageAccount.loanRate ?: 0.0

        remainMonth = loanManageAccount.remainRepayPeriod
        unredeemedPeriod = loanManageAccount.unredeemedPeriod ?: 0

        openDate = loanManageAccount.openDate!!
        dueDate = loanManageAccount.dueDate!!
        repayDay = loanManageAccount.repayDay ?: 0

        expectedRepayAmount = STANDARD_REPAY_MONEY
    }

    override fun toString(): String {
        return """
            은행명 $bankName
            대출 금액 $loanAmount
            금리 $monthlyRate
            대출방식 $loanType

            상환 전 이자 $interestBeforeRepay
            상환 후 이자 $interestAfterRepay
            총 감소 이자 $interestDiff

            상환 전 월 부담액 $monthlyAmountBeforeRepay
            상환 후 월 부담액 $monthlyAmountAfterRepay
            매월 감소 부담액 $monthlyAmountDiff

            여유자금 중 상환 금액 $expectedRepayAmount
        """.trimIndent()
    }

    companion object {
        private const val STANDARD_REPAY_MONEY = 10_000L
    }
}