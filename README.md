# Finda Calculator

- **Finda 앱**에서 실제 사용하고 있는 계산 로직들을 분리해 Kotlin 프로젝트로 제작한 Repository 입니다.

## 데이터셋
- 프로젝트 내에서 사용하고 있는 데이터들은 아래와 같습니다.
    - 상환금 예상 계산기

      - LoanCalculator를 선언하고 있는 부분에서 서버 데이터를 사용하는 부분은 yearlyRate 뿐입니다.

        ```
        _calculator.value = LoanCalculator().apply {
            yearlyRate = loanApply.value?.condition?.loanRate?.toFloat() ?: 0f
            // ...
            calculate()
        }
        ```

        LoanApply는 서버 통신을 통해서 가져온 대출 데이터이고, 금리는 가상으로 정할 수 있습니다.

        정하고 있는 데이터는 금리뿐이기 때문에 처음부터 가상의 `LoanCalcAccount` 객체를 생성해 계산합니다.

    - 홍민님 실제 대출 데이터 - 하단에서 설명

### 홍민님 실제 대출 데이터

- jsonString 형태로 LoanManageAccountExample.kt 파일에 저장되어 있고, `getLoanManageAccountObjectExample()` 함수로 `LoanManageAccount` 리스트를 가져올 수 있습니다.

```text
1. BNK경남은행
원금 3000만원
잔액 2763.7만원
금리 4.08%
상환방식 원금균등상환
거치기간 0개월
상환일 22일

2. 신한은행
원금 570만원
잔액 570만원
금리 2.69%
상환방식 일시상환
거치기간 0개월
상환일 26일

3. 국민은행
원금 1.18억원
잔액 6178.8만원
금리 2.5%
상환방식 원리금균등
거치기간 36개월
상환일 25일

4. BNK경남은행
원금 2200만원
잔액 2200만원
금리 3.75%
상환방식 일시상환
거치기간 0개월
상환일 26일

5. 카카오뱅크
원금 3800만원
잔액 2005.8만원
상환방식 마이너스통장
거치기간 0개월
상환일 5일
```

